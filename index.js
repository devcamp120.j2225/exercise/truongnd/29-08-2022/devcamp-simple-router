 // import  thư viện express.js vào. Dạng Import express from "express";
 const express = require('express');

 // khởi tạo app express
 const app = express();

 //khai báo sẵn 1 port trên hệ thống
 const port = 8000;

 // Khai báo API
app.get("/", (rep,res) => {
    let today = new Date();

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1 } năm ${today.getFullYear()}`
    })
});

// get method
app.get("/get-method", (rep,res) => {
    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Get Method`
    })
});
// Post method
app.post("/post-method", (rep,res) => {
    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Post Method`
    })
});
// Put method
app.put("/put-method", (rep,res) => {
    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Put Method`
    })
});
// Delete method
app.delete("/delete-method", (rep,res) => {
    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Delete Method`
    })
});
 // Khơi tạo app
 // Callback function
 app.listen(port, () =>{
    console.log("App listening on port: ",port);
 });